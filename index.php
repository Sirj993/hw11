<?php
session_start();
$template = [
    'templateData1' => 'goods',
    'templateData2' => 'rates',
    'templateTitle' => 'HW11',
    'templateDescription' => 'Hillel HW11. Functions.',
];
extract ($template);
require_once $_SERVER['DOCUMENT_ROOT'].'/data/'.$templateData1.'.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/data/'.$templateData2.'.php';
$userCart = [];
if (!empty($_COOKIE['user-cart'])){
$userCart = json_decode($_COOKIE['user-cart'], true);};

function getPriceWithDiscount($price_val, $discount_type, $discount_val){
    if ($discount_type == 'value'){
        $priceWDisc = $price_val - $discount_val;
    } elseif ($discount_type == 'percent'){
        $priceWDisc = $price_val * (100 - $discount_val) / 100;
    };
    return round($priceWDisc, 2);
};
function convertPrice($priceWDisc, $rate){
    $priceInCurrency = $priceWDisc / $rate;
    return number_format($priceInCurrency, 2); 
};
?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/basic/header.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/basic/links.php';?>

<div class="container">
    <form action="/currency.php" method="GET">
        <label>Choose currency:
            <select class="form-control" name="currency">
                <option value='uah'>uah</option>
                <option value='usd'>usd</option>
                <option value='eur'>eur</option>
            </select>    
            <div class="row">
                <button class="btn btn-success">Choose</button>
            </div>
        </label>
    </form>
</div>
<table class="table container ">
    <thead>
        <tr>
            <th>#</th>
            <th>item</th>
            <th>price in uah</th>
            <?php if ($_SESSION['currency'] != 'uah'): ?>
                <th>price in <?= $_SESSION['currency']; ?> </th>
            <?php endif ?>
            <th>in cart</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($goods as $num => $good): ?>
        <tr>
            <td><?=++$num ?></td>
            <td><?=$good['title']?></td>
            <td>
                <s><?= $good['price_val'] ?></s>
                <p class="discount">
                    <?= $priceWDisc = getPriceWithDiscount($good['price_val'], $good['discount_type'], $good['discount_val']); ?>
                </p>
            </td>
            <?php if ($_SESSION['currency'] != 'uah'): ?>
                <td class="discount"><?= convertPrice($priceWDisc, $currencyRate[$_SESSION['currency']]['rate']); ?></td>
            <?php endif ?>

            <td><?php if(!empty($userCart)): ?>
                <?php foreach ($userCart as $item => $count): ?>
                <?php if($item == $good['title']): ?>
                    <?= $userCart[$item] ?>
                <?php endif ?>
                <?php endforeach ?>
                <?php endif ?>
            </td>
            <td><a href="/cart_add.php?buy_item=<?=$good['title'] ?>">buy</a></td>
        </tr>
        <?php endforeach ?>
    </tbody>
</table>


<!-- tests -->
<!-- <div>
    <?= '<pre>';?>
    <?=print_r($good);?> 
    <?= '<pre>';?>
    <?=print_r($userCart);?>    
    <?= '<pre>';?>
    <?=print_r($_COOKIE['user-cart']);?>
</div> -->

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/basic/footer.php';?>